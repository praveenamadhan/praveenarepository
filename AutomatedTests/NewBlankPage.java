package com.atlassian.confluence.loginPage;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;

public class NewBlankPage {
	
	WebDriver driver;
	By SampleSpace = By.linkText("sample space");
	By Pages = By.linkText("Pages");
	By SampleSpaceHome = By.linkText("sample space Home");
	By Location = By.id("rte-button-location");
	By Lables = By.id("rte-button-labels");
	By Restrictions = By.id("rte-button-restrictions");
	By ToolBar = By.id("toolbar");
	By Preview = By.id("rte-button-preview");
	By Save = By.id("rte-button-publish");
	By Close = By.id("rte-button-cancel");
	By TitleHeading = By.id("title-heading");
	By TitleText = By.id("title-text");

			
	
	public NewBlankPage (WebDriver driver){
        this.driver = driver;
    }
	
	protected WebElement ContentTitle() {
		return driver.findElement(By.id("content-title"));
	}
	
	protected WebElement SaveButton() {
		return driver.findElement(Save);
	}
	
	protected WebElement CloseButton() {
		return driver.findElement(Close);
	}
	protected boolean isElementPresent(WebDriver wdr, By locator) {
        try {
          wdr.findElement(locator);
          return true;
        } catch (NoSuchElementException e) {
          return false;
        }
      }
	public int elementCount(WebDriver wdr, By locator){
		try{
		List Elements = wdr.findElements(locator);
		return Elements.size();
			
		} catch (Exception e)
		{
		 e.printStackTrace();
		 return -1;
		}
		
	}
	
}
