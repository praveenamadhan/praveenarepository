package com.atlassian.confluence.loginPage;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;
import org.junit.Test;

import com.atlassian.confluence.loginPage.LoginPageWebElements;
import com.atlassian.confluence.loginPage.ConfluenceHomePage;
import com.atlassian.confluence.loginPage.NewBlankPage;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
/**
 * @author praveenp
 *
 */

public class ValidateCreateNewPage {
	
// Declare the variables 
WebDriver driver;
LoginPageWebElements objLogin;
ConfluenceHomePage objHomePage;
NewBlankPage objBlankPage;
String title = "";
				
/**
 * This function opens the base url and performs a login operation with the provided username and password.This is the pre-requiste for each test and hence written under the Before annotation
 * @param - none
 * @return - none
 * @throws Exception
 */
@Before	
public void setUp() throws Exception
{
	driver = new FirefoxDriver();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
	driver.get("https://praveena.atlassian.net");
    objLogin = new LoginPageWebElements(driver);
    objLogin.loginToConf("praveena.palaniswamy@gmail.com", "Krishna@82");
    objHomePage= new ConfluenceHomePage(driver);
    objBlankPage = new NewBlankPage(driver);
}
    
/**
 * This function is to verify if the "create" link is present on the home/landing page of confluence.
 * @param - none
 * @return - none
 */

@Test
public void testExistenceOfCreateLink()
{
	By locator = objHomePage.CreateLink;
	try {
		Assert.assertTrue("Create link is not present", objHomePage.isElementPresent(driver,locator));
	} catch (Exception e) {
		e.printStackTrace();
	}
	driver.close();
}  
	
/**
 * To Verify if the dialog box with multiple template options is displayed on clicking the �Create� link.. This is verified using the title of the page displayed
 * @param - none
 * @return - none
 */
@Test
public void testCreatePageDialogBox()
{
	objHomePage.Create().click();

	try{
		Assert.assertTrue("Dialog Box title is does not match",objHomePage.DialogTitle().getText().contentEquals("Help\nCreate"));
	} catch (Exception e)
	{
		e.printStackTrace();
	}
	driver.close();
}

/**
 * To Verify if the "Blank Page" is displayed after choosing the Blank page option and create button in the dialogbox. This is verified using the title of the page displayed
 * @param - none
 * @return - none
 */

@Test
public void testCreateNewPage()
{
	objHomePage.CreateNewPage();
	try{
		Assert.assertTrue("Title does not match,create dialog box is not displayed",driver.getTitle().contentEquals("Add Page - sample space - Confluence"));
	} catch (Exception e){
		e.printStackTrace();
	}
	driver.close();
} 
	
	
/**
 * To Verify if the three links <space> <pages> <space home> are present in the blank page
 * @param - none
 * @return - void
 */

@Test
public void testLinksInNewPage() 
{
	try{
			objHomePage.CreateNewPage();
			Assert.assertTrue("space link does not Exist" ,objBlankPage.isElementPresent(driver, objBlankPage.SampleSpace));
			Assert.assertTrue("space home link does not Exist" ,objBlankPage.isElementPresent(driver, objBlankPage.SampleSpaceHome));
			Assert.assertTrue("Pages link does not Exist" ,objBlankPage.isElementPresent(driver, objBlankPage.Pages));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		driver.close();
	}

/**
 * To Verify if the three icons location,labels and restrictions are present in the blank page
 * @param - none
 * @return - void
 */

@Test
public void testIconsInNewPage() 
{
	try{
			objHomePage.CreateNewPage();
			Assert.assertTrue("Location icon is not present" ,objBlankPage.isElementPresent(driver, objBlankPage.Location));
			Assert.assertTrue("Labels icon is not present" ,objBlankPage.isElementPresent(driver, objBlankPage.Lables));
			Assert.assertTrue("Restrictions icon is not present" ,objBlankPage.isElementPresent(driver, objBlankPage.Restrictions));
		
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		driver.close();
	}
/**
 * To Verify if the three buttons "preview, save and close" are present and also verify if tool bar is present in the new blank page
 * @param - none
 * @return - void
 */

@Test
public void testButtonsAndToolBarInNewPage() 
{
	try{
			objHomePage.CreateNewPage();
			Assert.assertTrue("tool bar does not exist" ,objBlankPage.isElementPresent(driver, objBlankPage.ToolBar));
			Assert.assertTrue("Preview button does not exist" ,objBlankPage.isElementPresent(driver, objBlankPage.Preview));
			Assert.assertTrue("Save Button does not exist" ,objBlankPage.isElementPresent(driver, objBlankPage.Save));
			Assert.assertTrue("Close Button does not exist" ,objBlankPage.isElementPresent(driver, objBlankPage.Close));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		driver.close();
	}
/**
 * To verify if the newly created page exists after clicking the SAVE button
 * @param - none
 * @return - void
 */

@Test
public void testFunctionalityOfSaveButtonInNewPage() 
{
	objHomePage.CreateNewPage();
	objBlankPage.ContentTitle().clear();
    objBlankPage.ContentTitle().sendKeys("praveena page");
    objBlankPage.SaveButton().click();
    try {
    	Assert.assertTrue("Created page does not exist",objBlankPage.isElementPresent(driver,objBlankPage.TitleText));
    } catch (Error e) {
    	e.printStackTrace();
    	
    }
    driver.close();
 }
/**
 * To verify if the blank page is discarded on clicking the close button 
 * @param - none
 * @return - void
 */

@Test
public void testFunctionalityOfCloseButton()
{
	objHomePage.CreateNewPage();
	objBlankPage.ContentTitle().clear();
    objBlankPage.ContentTitle().sendKeys("random page");
    objBlankPage.CloseButton().click();
    try
    {
    	Assert.assertTrue(objHomePage.getHomePageDashboardName().contentEquals("Welcome to Confluence"));
    } catch(Exception e)
    {
    	e.printStackTrace();
    }
    driver.close();
}

}