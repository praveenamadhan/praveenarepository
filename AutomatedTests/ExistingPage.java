package com.atlassian.confluence.loginPage;


import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;


public class ExistingPage {

	WebDriver driver;
	By Restrictions = By.id("rte-button-restrictions");
	By RestrictViewing = By.id("restrictViewRadio");
	By RestrictEditing = By.id("restrictEditRadio");
	By PagePerChooseMe = By.linkText("Me");
	By PagePerChooseUser = By.id("page-permissions-choose-user");
	By PagePerChooseGroup = By.id("page-permissions-choose-group");
	By TextBox = By.id("page-permissions-names-input");
    By Restrict = By.id("add-typed-names");
    By SaveRestriction = By.cssSelector("button.button-panel-button.permissions-update-button");
    By Save = By.id("rte-button-publish");
    By Close = By.linkText("Close");
	By Dialog = By.cssSelector("#update-page-restrictions-dialog > div.dialog-components > h2.dialog-title");
	By RestrictionAppliedIcon = By.id("content-metadata-page-restrictions");
	public ExistingPage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public boolean isElementPresent(WebDriver wdr, By locator) {
        try {
          wdr.findElement(locator);
          return true;
        } catch (NoSuchElementException e) {
          return false;
        }
      }
	protected WebElement RestrictEdit(){
		return driver.findElement(RestrictEditing);
	}
	protected WebElement DialogTitle ()
	{
		return driver.findElement(Dialog);
	}
	protected WebElement RestrictiView(){
		return driver.findElement(RestrictViewing);
	}
	protected WebElement MeButton(){
		return driver.findElement(PagePerChooseMe);
	}
	protected WebElement SaveRestrictionButton(){
		return driver.findElement(SaveRestriction);
	}
	protected WebElement SaveButton(){
		return driver.findElement(Save);
	}
	protected WebElement RestrictionsButton(){
		return driver.findElement(Restrictions);
	}
	public boolean isEnabled(WebDriver wdr, By Locator) 
	{
		try{
			return wdr.findElement(Locator).isEnabled();
			
		} catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean isChecked(WebDriver wdr, By Locator) 
	{
		try{
			return wdr.findElement(Locator).isSelected();
			
		} catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	public void clickRestrictions(){
		try{
			driver.findElement(Restrictions).click();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
