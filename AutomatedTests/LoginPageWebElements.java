package com.atlassian.confluence.loginPage;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;

public class LoginPageWebElements {

	WebDriver driver;
	By userName = By.name("username");
	By password = By.name("password");
	By login = By.id("login");
	//By title= By.id("com-atlassian-confluence");
	
	public LoginPageWebElements(WebDriver driver)
	{
		this.driver = driver;
	}
	public void setUsername(String strusername)
	{
		driver.findElement(userName).sendKeys(strusername);
	}
	public void setPassword(String strpasswd)
	{
		driver.findElement(password).sendKeys(strpasswd);
	}
	public void clickLogin()
	{
		driver.findElement(login).click();
	}
	public void loginToConf(String strUserName,String strPasword)
	{
		 
        //Fill user name
 
        this.setUsername(strUserName);
 
        //Fill password
 
        this.setPassword(strPasword);
 
        //Click Login button
 
        this.clickLogin();        
         }
}
