package com.atlassian.confluence.loginPage;

import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;
import org.junit.Test;


import com.atlassian.confluence.loginPage.LoginPageWebElements;
import com.atlassian.confluence.loginPage.ExistingPage;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
public class ValidateExistingPageRestrictions 
{
	WebDriver driver;
	LoginPageWebElements objLogin;
	ExistingPage objExistingPage;

	
/**
 * This function opens the base url and performs a login operation with the provided username and password.This is the pre-requiste for each test and hence written under the Before annotation
 * @param - none
 * @return - none
 */
@Before
public void setup()
{
	driver = new FirefoxDriver();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    driver.get("https://praveena.atlassian.net");
    objLogin = new LoginPageWebElements(driver);
    objLogin.loginToConf("praveena.palaniswamy@gmail.com", "Krishna@82");
    driver.get("https://praveena.atlassian.net/wiki/pages/editpage.action?pageId=491534");
    objExistingPage = new ExistingPage(driver);
}
/**
 * This function is to test if the restrictions icon is present on the existing page and is enabled
 * @param - none
 * @return - none
 */
@Test
public void testIsRestrictionsIconPresentandEnabled()
{
	try{
		Assert.assertTrue("Restrictions icon is present" ,objExistingPage.isElementPresent(driver, objExistingPage.Restrictions));
		Assert.assertTrue("Restrictions icon not enabled" ,objExistingPage.isEnabled(driver, objExistingPage.Restrictions));
		} catch(Exception e){
		e.printStackTrace();
		}
		driver.close();
}
	
/**
 * This function is to test if the restrictions dialog box is displayed on clicking the restrictions icon
 * @param - none
 * @return - none
 */
@Test
public void testClickRestrictions()
{
	objExistingPage.clickRestrictions();
	try{
		Assert.assertTrue("Restrictions dialog box exists",objExistingPage.isElementPresent(driver, objExistingPage.Dialog));
	} catch (Exception e)
	{
		e.printStackTrace();
	}
	driver.close();
} 

/**
 * This function is to test if the restrictions dialog box is displayed with 2 radio buttons "Restrict Edit" and "Restrict View"
 * @param - none
 * @return - none
 */
@Test
public void testRestrictionRadioButtonsDisplayed()
{
	objExistingPage.clickRestrictions();
	try
	{
		Assert.assertTrue("Restrict Edit Radio Button does not Exist",objExistingPage.isElementPresent(driver, objExistingPage.RestrictEditing));
		//Assert.assertTrue("Restrict Edit Radio Button enabled",ObjExistingPage.isChecked(driver, ObjExistingPage.RestrictEditing));
		Assert.assertTrue("Restrict View Radio Button does not Exist",objExistingPage.isElementPresent(driver, objExistingPage.RestrictViewing));
	} catch (Exception e){
		e.printStackTrace();
	}
		driver.close();
}

/**
 * This function is to test if the permission buttons "Me", "user","Group" are dispalyed along with a input text box and "restrict" button
 * @param - none
 * @return - none
 */
@Test
public void testPermissionButtonsDisplayed()
{
	objExistingPage.clickRestrictions();
	try
	{
		Assert.assertTrue("Permission Me button does not Exist",objExistingPage.isElementPresent(driver, objExistingPage.PagePerChooseMe));
		Assert.assertTrue("Permission User button does not Exist",objExistingPage.isElementPresent(driver, objExistingPage.PagePerChooseUser));
		Assert.assertTrue("Permission Group button does not Exist",objExistingPage.isElementPresent(driver, objExistingPage.PagePerChooseGroup));
		Assert.assertTrue("Text box Exist",objExistingPage.isElementPresent(driver, objExistingPage.TextBox));
		Assert.assertTrue("Restrict Button does not Exist",objExistingPage.isElementPresent(driver, objExistingPage.Restrict));
	} catch (Exception e){
		e.printStackTrace();
	}
		driver.close();
}

/**
 * This function is to test if the Save and Close Buttons are present. Also checks if the Save Button is disabled by default
 * @param - none
 * @return - none
 */
 @Test
public void testSaveAndCloseButtonsDisplayed()
{
	objExistingPage.clickRestrictions();
	try
	{
		Assert.assertTrue("Save button does not Exist",objExistingPage.isElementPresent(driver, objExistingPage.SaveRestriction));
		Assert.assertTrue("Save button is not disabled",driver.findElement(objExistingPage.SaveRestriction).isEnabled());
		Assert.assertTrue("Close link does not Exist",objExistingPage.isElementPresent(driver, objExistingPage.Close));
	} catch (Exception e){
		e.printStackTrace();
	}
		driver.close();
}

/**
 * This function is to test if the restrict edit functionality works successfully.
 * The Edit Button should be disabled  for this page for the logged in user.This should be the ideal step of verification
 * But because the page is still accessible for edit, the verification usd is to check for the "restricted" icon present on the top of the page name. This is not present for pages that are not restricted.
 * @param - none
 * @return - none
 */
@Test
public void testRestrictEditFunctionality()
{
	objExistingPage.clickRestrictions();
	objExistingPage.RestrictEdit().click();
	objExistingPage.MeButton().click();
	objExistingPage.SaveRestrictionButton().click();
	objExistingPage.SaveButton().click();
	try
	{
		Assert.assertTrue("Did not return back to expected page",driver.getTitle().contains("My second page - sample space - Confluence"));
		Assert.assertTrue("The restricted icon in red is not present",objExistingPage.isElementPresent(driver, objExistingPage.RestrictionAppliedIcon));
	} catch (Exception e){
		e.printStackTrace();
	}
		driver.close();
}

/**
 * This function is to test if the restrict view functionality works successfully.
 * The page should be no longer be visible or disabled for the logged in user. This is the ideal step for verification
 * But because the page is still accessible for edit, the verification usd is to check for the "restricted" icon present on the top of the page name. This is not present for pages that are not restricted.
 * @param - none
 * @return - none
 */
@Test
public void testRestrictViewFunctionality()
{
	objExistingPage.clickRestrictions();
	objExistingPage.RestrictiView().click();
	objExistingPage.MeButton().click();
	objExistingPage.SaveRestrictionButton().click();
	objExistingPage.SaveButton().click();
	try
	{
		Assert.assertTrue("Did not return back to expected page",driver.getTitle().contains("My second page - sample space - Confluence"));
		Assert.assertTrue("The restricted icon in red is not present",objExistingPage.isElementPresent(driver, objExistingPage.RestrictionAppliedIcon));
	} catch (Exception e){
		e.printStackTrace();
	}
		driver.close();
}

}
