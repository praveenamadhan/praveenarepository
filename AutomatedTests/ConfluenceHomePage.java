package com.atlassian.confluence.loginPage;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;

public class ConfluenceHomePage {

    WebDriver driver;
    By homePageDisplayName = By.id("WelcometoConfluence");
    By CreateLink = By.linkText("Create");
    By BlankPageTemplate = By.cssSelector("li.template.selected");
    By TemplateDescription = By.cssSelector("div.template-description");
    By CreateButton = By.xpath("//div[@id='create-dialog']/div/div[2]/button");
    By Dialog = By.cssSelector("h2.dialog-title");
    
    public ConfluenceHomePage(WebDriver driver){
         this.driver = driver;
     }
 
    protected String getHomePageDashboardName() throws Exception {
 
         return driver.findElement(homePageDisplayName).getText();
    }
    
    protected WebElement Create(){
    	return driver.findElement(CreateLink);
    }
    
    protected WebElement DialogTitle (){
    	return driver.findElement(Dialog);
    }
         
    public boolean isElementPresent(WebDriver wdr, By locator) {
        try {
          wdr.findElement(locator);
          return true;
        } catch (NoSuchElementException e) {
          return false;
        }
      }
    
     public void CreateNewPage(){
    	driver.findElement(CreateLink).click();
        driver.findElement(BlankPageTemplate).click();
        driver.findElement(TemplateDescription).click();
        driver.findElement(CreateButton).click();
    	
    }
}
         
      
