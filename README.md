# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for uploading the automated and exploratory tests

* There are 2 folders that has been uploaded.
### **1. AutomatedTests:** 
This folder contains the source files, readme and results snapshot

**Source files:**

a.ConfluenceHomePage.java

b.LoginPageWebElements.java

c.NewBlankPage.java

d.ValidateCreateNew.java

e.ExistingPage.java

f.ValidateExistingPageRestrictions.java


**ReadMe file:** ReadMeForAutomationTests.docx.
This file describes in detail, the assumptions, tests, design and issues in detail

**Results:**
Two snapshots file which shows that execution has been successfull

### **2. ExploratoryTesting** ###
This folder contains the 

1. **Exploratory Testing.docx** - that details all the testing done with results and bugs

2. A list of snapshots of the Bugs found